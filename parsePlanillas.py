#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 16 10:06:21 2022

@author: seba
"""
# %% imports and load files
import pandas as pd
import numpy as np
import re

from unidecode import unidecode

activos_file = "planillas/Activos.xlsx"
movimientos_file = "planillas/SaldMovs_.xls" # replace here with the correct filename

activ = pd.read_excel(activos_file)
movim = pd.read_excel(movimientos_file)

# %%
# nombres en uppercase
nombresCompletos = np.array(activ[["Nombre", "Apellido"]])
nombresCompletos = [" ".join(nom).replace("  ", " ") for nom in nombresCompletos]
nombresCompletos.append("Sin Nombre")
nombresCompletos.sort()
print(nombresCompletos)

nombresSet = [set(unidecode(nom, "utf-8").replace(".", " ").upper().split()) for nom in nombresCompletos]

# crear diccionario para meter los movimientos de cada socio
estado = {}
for nom in nombresCompletos:
    estado[nom] = [] # dos listas fvacias, para fecha y credito

j = 0
for mv in movim.itertuples():
    # mv = movim.loc[i].T
    # print(mv)
    con = mv.Concepto

    if isinstance(con, str) and "Transf" in con:
        con = unidecode(con.upper().replace("-", " ").replace(".", " ").replace(":", " ").replace(",", " "), "utf-8")
        con = con.split()[6:]

        intersecs = [len(set(con).intersection(nom)) for nom in nombresSet]
        s = np.argmax(intersecs)

        if intersecs[s] == 0:
            nom = "Sin Nombre"
        else:
            nom = nombresCompletos[s]
        print(mv.Fecha, mv.Crédito, nom)
        estado[nom].append([mv.Fecha, mv.Crédito])

        # print(mv)
        # estado[nom] = pd.concat([estado[nom], mv])
        j = j+1

        # ver si titul coincide con algun nombre

print("Cantidad de movimientos: ", j)

# %%

for nom in nombresCompletos:
    if len(estado[nom]) > 0 and nom != "Sin Nombre":
        print("SOCIO: ", nom)
        for est in estado[nom]:
            print(*est)
        print()
